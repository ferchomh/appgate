# appgate prueba tecnica

prueba tecnica appgate - Adicional se sube PDF con todas las imagenes de lo realizado 
Respuestas-PruebaTecnicaLuisMejia.pdf

## 1- Cree una VPC con el siguiente segmento de red: 10.1.96.0/24. Teniendo eso, cree 5 subredes en diferentes AZ cada una; 3 deben ser privados y los otros 2 públicos. todo esto debe hacerse usando Terraform.
Subnetting utilizado.
----
Address:    	10.1.96.10           	00001010.00000001.01100000. 00001010
Netmask:	255.255.255.0 = 24	11111111.11111111.11111111. 00000000
Wildcard:	0.0.0.255	00000000.00000000.00000000. 11111111
=>
Network:    	10.1.96.0/24         	00001010.00000001.01100000. 00000000
HostMin:	10.1.96.1	00001010.00000001.01100000. 00000001
HostMax:	10.1.96.254	00001010.00000001.01100000. 11111110
Broadcast:	10.1.96.255	00001010.00000001.01100000. 11111111
Hosts/Net:	254	Class A, Private Internet
Subnets after transition from /24 to /27
Netmask:    	255.255.255.224 = 27 	11111111.11111111.11111111.111 00000
Wildcard:	0.0.0.31	00000000.00000000.00000000.000 11111
1.
Network:    	10.1.96.0/27         	00001010.00000001.01100000.000 00000
HostMin:	10.1.96.1	00001010.00000001.01100000.000 00001
HostMax:	10.1.96.30	00001010.00000001.01100000.000 11110
Broadcast:	10.1.96.31	00001010.00000001.01100000.000 11111
Hosts/Net:	30	Class A, Private Internet
2.
Network:    	10.1.96.32/27        	00001010.00000001.01100000.001 00000
HostMin:	10.1.96.33	00001010.00000001.01100000.001 00001
HostMax:	10.1.96.62	00001010.00000001.01100000.001 11110
Broadcast:	10.1.96.63	00001010.00000001.01100000.001 11111
Hosts/Net:	30	Class A, Private Internet
3.
Network:    	10.1.96.64/27        	00001010.00000001.01100000.010 00000
HostMin:	10.1.96.65	00001010.00000001.01100000.010 00001
HostMax:	10.1.96.94	00001010.00000001.01100000.010 11110
Broadcast:	10.1.96.95	00001010.00000001.01100000.010 11111
Hosts/Net:	30	Class A, Private Internet
4.
Network:    	10.1.96.96/27        	00001010.00000001.01100000.011 00000
HostMin:	10.1.96.97	00001010.00000001.01100000.011 00001
HostMax:	10.1.96.126	00001010.00000001.01100000.011 11110
Broadcast:	10.1.96.127	00001010.00000001.01100000.011 11111
Hosts/Net:	30	Class A, Private Internet
5.
Network:    	10.1.96.128/27       	00001010.00000001.01100000.100 00000
HostMin:	10.1.96.129	00001010.00000001.01100000.100 00001
HostMax:	10.1.96.158	00001010.00000001.01100000.100 11110
Broadcast:	10.1.96.159	00001010.00000001.01100000.100 11111
Hosts/Net:	30	Class A, Private Internet
6.
Network:    	10.1.96.160/27       	00001010.00000001.01100000.101 00000
HostMin:	10.1.96.161	00001010.00000001.01100000.101 00001
HostMax:	10.1.96.190	00001010.00000001.01100000.101 11110
Broadcast:	10.1.96.191	00001010.00000001.01100000.101 11111
Hosts/Net:	30	Class A, Private Internet
7.
Network:    	10.1.96.192/27       	00001010.00000001.01100000.110 00000
HostMin:	10.1.96.193	00001010.00000001.01100000.110 00001
HostMax:	10.1.96.222	00001010.00000001.01100000.110 11110
Broadcast:	10.1.96.223	00001010.00000001.01100000.110 11111
Hosts/Net:	30	Class A, Private Internet
8.
Network:    	10.1.96.224/27       	00001010.00000001.01100000.111 00000
HostMin:	10.1.96.225	00001010.00000001.01100000.111 00001
HostMax:	10.1.96.254	00001010.00000001.01100000.111 11110
Broadcast:	10.1.96.255	00001010.00000001.01100000.111 11111
Hosts/Net:	30	Class A, Private Internet
Subnets: 8
Hosts: 240
----
Se deja el maint_subredes.tf con el codigo utilizo para este paso.

![image.png](./image.png)

![image-1.png](./image-1.png)

![image-2.png](./image-2.png)

## 2- Cree un EC2 t3.micro con Amazon Linux 2. Este EC2 debe tener discos con 10 GB Gp2 donde el disco se asocia como partición al “/data_test”; todo esto debe hacerse usando Terraform.

Se deja el maint_subredes.tf con el codigo utilizo para este paso.

En resumen
---------------------------------------------------------
 provider "aws" {
	access_key = "AKIAWNP6MXA5ODIYO3XX"
	secret_key = "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"
	region = "us-east-2"
}

# EC2 Webserver
resource "aws_instance" "my-instance" {
	ami = "ami-0231217be14a6f3ba"
	instance_type = "t3.micro"
  security_groups = ["${aws_security_group.allow_http.id}"]
  subnet_id = "${aws_subnet.public-subnet1.id}"
	#user_data = "${file("install_apache.sh")}"
	tags = {
		Name = "EC2 con Volume GP2"
	}
}

# step: 2  --> creating volume
	resource "aws_ebs_volume" "myvol" {
	  availability_zone = aws_instance.my-instance.availability_zone 
	  size = 10
	 }
	
# step: 3 --> attaching ebs to os
	resource "aws_volume_attachment" "ebs_att" {
	  device_name = "/dev/sdh"
	  volume_id = aws_ebs_volume.myvol.id
	  instance_id = aws_instance.my-instance.id
	}

---------------------------------------------------------

## 3- Implemente un servidor web Apache en el EC2 creado en el paso 2. Considere usar Docker para este despliegue.

---------------------------------------------------------
provider "aws" {
	access_key = "AKIAWNP6MXA5ODIYO3XX"
	secret_key = "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"
	region = "us-east-2"
}

resource "aws_instance" "InstanceDocker" {
	ami = "ami-0231217be14a6f3ba"
	instance_type = "t2.micro"
 
	user_data = <<-EOF
			#!/bin/bash
			set -ex
			sudo yum update -y
			sudo amazon-linux-extras install docker -y
			sudo service docker start
			sudo usermod -a -G docker ec2-user
			sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
			sudo chmod +x /usr/local/bin/docker-compose
			EOF
		tags = {
		Name = "My Instance Docker EC2 using Terraform"
	}
	vpc_security_group_ids = [aws_security_group.instance.id]
}
 
resource "aws_security_group" "instance" {
	
 	ingress {
		from_port = 80
		to_port = 80
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	ingress {
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
 
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
---------------------------------------------------------

![image-3.png](./image-3.png)

![image-4.png](./image-4.png)

Instalamos el httpd en Docker
#sudo docker run -dit --name tecmint-web -p 8080:80 -v /home/user/website/:/usr/local/apache2/htdocs/ httpd:2.4

Nos conectamos a la nueva imagen desplegada y creamos en .html
#vi /home/user/website/docker.html
![image-5.png](./image-5.png)

Revisamos por navegador web.
![image-6.png](./image-6.png)


## 4- Cree un script sh que recopile el total de las conexiones abiertas, en el desplegado Servidor web Apache en el paso 3, para cada hora del día en curso. Considere usar el Apache registra archivos como su fuente de datos.

Conectamos en la imagen que creamos de httpd
# crontab -e
##Revisa #Conexiones Apache
01 */1 * * * sh /data_test/scripts/leer_cnx_httpd.sh
otra manera
01 0-23 * * * sh /data_test/scripts/leer_cnx_httpd.sh

# vi /data_test/scripts/leer_cnx_httpd.sh
----------------------------------------
echo -e '\n'
sudo date   >> /var/www/html/index2.html
sudo awk '{!a[$1]++}END{for(i in a) if ( a[i] >10 ) print a[i],i }' /var/log/httpd/access_log >> /var/www/html/index2.html
----------------------------------------

## 5- Cree una tarea programada de Linux que ejecute el script del paso 4 cada minuto.
# crontab -e
*/4 * * * * sh /data_test/scripts/monitoreo_cnx.sh
otra manera
0,4,8,12,16,20,24,28,32,36,40,44,48,52,56 * * * * sh /data_test/scripts/monitoreo_cnx.sh


## 6- Desarrolle una página web que muestre la información reunida en el paso 4 Esta página web debe implementarse en el contenedor de Apache desde el paso 3.

Con el script generado en los pasos anteriores se envia un sudo awk '{!a[$1]++}END{for(i in a) if ( a[i] >10 ) print a[i],i }' /var/log/httpd/access_log >> /var/www/html/index2.html

Este script lo que hace es pintar los resultados en texto plano.

![image-7.png](./image-7.png)


## 7- Ejecute el paso 2 nuevamente para crear una segunda instancia EC2; debe tener las mismas especificaciones. Implemente la página web desarrollada en el paso 6 en este nuevo EC2. El balanceador de carga solo debe responder a la solicitud de páginas web a través del puerto 80 con HTTP. B. El balanceador de carga solo debe permitir solicitudes de su dirección IP. C. La página web debe exponerse a través de la ruta “/appgate/testpage”, cualquier otra solicitud diferente debe responder con un error prohibido personalizado que dice: "Acceso no autorizado a los servicios de AppGate".

Se crea el Load Balancer 
 
Creamos el Healt check setting

Probamos desde navegador: http://loadbalance7-451719835.us-east-2.elb.amazonaws.com/

Probamos indicando el path 
http://loadbalance7-451719835.us-east-2.elb.amazonaws.com/appgate/testpage/

 
 ## 8- Cree un certificado autofirmado con su nombre. Ejemplo: jonhdoe.co. Configure el balanceador de carga, creado en AWS en el paso 8, para que ahora responda a la solicitud de servicios web a través de un agente de escucha en el puerto 443 con HTTPS habilitado.

Con openssl creamos el certificado para este paso.

# openssl req -newkey rsa:2048 \
>             -x509 \
>             -sha256 \
>             -days 365 \
>             -nodes \
>             -out luismejia.crt \
>             -keyout luismejia.key
Generating a 2048 bit RSA private key
...................+++ .............................................................................................................+++
writing new private key to 'luismejia.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:CO
State or Province Name (full name) []:BOGOTA
Locality Name (eg, city) [Default City]:BOGOTA
Organization Name (eg, company) [Default Company Ltd]:luismejia
Organizational Unit Name (eg, section) []:luismejia
Common Name (eg, your name or your server's hostname) []:luismejia.co
Email Address []:ferchomh@gmail.com
Se carga el certificado autofirmado en AWS.
 
Se crea la nueva entrada en Listener de Load Balancer para utilizar HTTPS.
 
Se crea la nueva entrada en security grupos de permitir acceso por HTTPS.

Se hacen pruebas desde el navegador web.
https://loadbalance7-451719835.us-east-2.elb.amazonaws.com/appgate/testapage/



## Práctica 3 - Estamos ejecutando una aplicación Python Flask (el código es irrelevante para este ejercicio, puede ser válido) usando gunicorn y haproxy dentro del mesmo container. O dockerfile es como mostramos:

Revisiones:
•	Se Corrige el texto de los "echo" con espacio.
•	Se agrega la instalación del paquete haproxy.
•	Se dividen los archivos para su prueba (dockerfile, docker_entrypoint.sh, haproxy.sh).
•	Se cambia el valor delivery_cloud:9000 por 127.0.0.1:9000. 

#vi dockerfile
FROM alpine
RUN apk add py3-pip build-base python3-dev libffi-dev openssl-dev
RUN apk add nginx haproxy
RUN mkdir -p /opt/api
WORKDIR /opt/api
ADD api/requirements.txt /opt/api
RUN pip3 install --no-cache-dir -r requirements.txt
ADD api/. /opt/api
ADD ./docker-entrypoint.sh /bin/docker-entrypoint
ADD ./haproxy.conf /etc/haproxy/haproxy.cfg
EXPOSE 80
CMD ["/bin/docker-entrypoint"]

# vi docker-entrypoint.sh
#!/bin/sh
echo "Starting gunicorn..."
gunicorn -w 5 -b 127.0.0.1:9000 app:app --daemon
sleep 3

echo "Starting haproxy..."
haproxy -f "/etc/haproxy/haproxy.cfg" &

# vi haproxy.cfg
global
maxconn 8192
defaults
log stdout format raw local0
mode http
option httplog
option forwardfor
option httpclose
option dontlognull
timeout connect 10s
timeout client 150s
timeout server 150s
errorfile 400 /usr/local/etc/haproxy/errors/400.http
errorfile 403 /usr/local/etc/haproxy/errors/403.http
errorfile 408 /usr/local/etc/haproxy/errors/408.http
errorfile 500 /usr/local/etc/haproxy/errors/500.http
errorfile 502 /usr/local/etc/haproxy/errors/502.http
errorfile 503 /usr/local/etc/haproxy/errors/503.http
errorfile 504 /usr/local/etc/haproxy/errors/504.http
maxconn 8192
frontend app-http
bind *:80
acl is_app path_beg -i /
use_backend flask_backend if is_app
backend flask_backend
server docker-app 127.0.0.1:9000 check verify none

Prueba de imagen - comandos utilizados
# docker build -t dockerfile .
#docker image ls
#docker run -itd -publish 6060:80 047d39ba914b
# docker exec -it 075cb8b1ca57 /bin/sh





