provider "aws" {
	access_key = "AKIAWNP6MXA5ODIYO3XX"
	secret_key = "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"
	region = "us-east-2"
}

# Variables

variable "vpc-name" {
    description = "El nombre de la vpc a crear"
    type    = string
    default = "mi-vpc-appgate"
}

variable "vpc-cidr" {
    description = "La dirección de red de la vpc a crear"
    type    = string
    default = "10.1.96.0/24"
}

variable "igw-name" {
    description = "Nombre del Internet Gateway"
    type    = string
    default = "mi-igw-appgate"
}

variable "public-subnet1-name" {
    description = "Nombre de la subred publica 1"
    type    = string
    default = "public-subnet1"
}

variable "public-subnet1-cidr" {
    description = "Rango de direcciones de la subred pública 1"
    type    = string
    default = "10.1.96.0/27"
}

variable "public-subnet2-name" {
    description = "Nombre de la subred publica 2"
    type    = string
    default = "public-subnet2"
}

variable "public-subnet2-cidr" {
    description = "Rango de direcciones de la subred publica 2"
    type    = string
    default = "10.1.96.32/27"
}

variable "private-subnet1-name" {
    description = "Nombre de la subred privada 1"
    type    = string
    default = "private-subnet1"
}

variable "private-subnet1-cidr" {
    description = "Rango de direcciones de la subred privada 1"
    type    = string
    default = "10.1.96.64/27"
}

variable "private-subnet2-name" {
    description = "Nombre de la subred privada 2"
    type    = string
    default = "private-subnet2"
}

variable "private-subnet2-cidr" {
    description = "Rango de direcciones de la subred privada 2"
    type    = string
    default = "10.1.96.96/27"
}

variable "private-subnet3-name" {
    description = "Nombre de la subred privada 3"
    type    = string
    default = "private-subnet3"
}

variable "private-subnet3-cidr" {
    description = "Rango de direcciones de la subred privada 3"
    type    = string
    default = "10.1.96.128/27"
}

variable "route-table-public-subnet1-name" {
    description = "Nombre de la tabla de rutas para la subred publica"
    type    = string
    default = "route-table-public-subnet1"
}

variable "route-table-public-subnet2-name" {
    description = "Nombre de la tabla de rutas para la subred publica"
    type    = string
    default = "route-table-public-subnet2"
}

variable "eip-name" {
    description = "Nombre de la eip para el nat gateway"
    type    = string
    default = "eip-ngw"
}

variable "ngw-name" {
    description = "Nombre para el nat gateway"
    type    = string
    default = "ngw"
}

# Recursos

# VPC

resource "aws_vpc" "main" { # Se exportan los atributos del recurso, pudiendose utilizar después
  cidr_block = var.vpc-cidr
  tags = {
    Name = var.vpc-name
  }
}

# Recoger zonas de disponibilidad disponibles

data "aws_availability_zones" "available" {
  state = "available"
}

# Subnet publica 1 (us-east-1)

resource "aws_subnet" "public-subnet1" {
  vpc_id     = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = var.public-subnet1-cidr
  map_public_ip_on_launch = true # auto-assing public ip
  tags = {
    Name = var.public-subnet1-name
  }
}

# Subnet publica 2 (us-east-2)

resource "aws_subnet" "public-subnet2" {
  vpc_id     = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = var.public-subnet2-cidr
  map_public_ip_on_launch = true # auto-assing public ip
  tags = {
    Name = var.public-subnet2-name
  }
}

# Subnet privada 1 (us-east-1)

resource "aws_subnet" "private-subnet1" {
  vpc_id     = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = var.private-subnet1-cidr

  tags = {
    Name = var.private-subnet1-name
  }
}

# Subnet privada 2 (us-east-2)

resource "aws_subnet" "private-subnet2" {
  vpc_id     = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = var.private-subnet2-cidr

  tags = {
    Name = var.private-subnet2-name
  }
}

# Subnet privada 3 (us-east-2)

resource "aws_subnet" "private-subnet3" {
  vpc_id     = "${aws_vpc.main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = var.private-subnet3-cidr

  tags = {
    Name = var.private-subnet3-name
  }
}

# IGW

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}" # Id exportado en la creación de la vpc

  tags = {
    Name = var.igw-name
  }
}

# Tabla de rutas para la subred pública 1

resource "aws_route_table" "route-table-public-subnet1" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = {
    Name = var.route-table-public-subnet1-name
  }
}

resource "aws_route_table_association" "route-table-association-public-subnet1" {
  subnet_id      = "${aws_subnet.public-subnet1.id}"
  route_table_id = "${aws_route_table.route-table-public-subnet1.id}"
}

# Tabla de rutas para la subred pública 2

resource "aws_route_table" "route-table-public-subnet2" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  tags = {
    Name = var.route-table-public-subnet2-name
  }
}

resource "aws_route_table_association" "route-table-association-public-subnet2" {
  subnet_id      = "${aws_subnet.public-subnet2.id}"
  route_table_id = "${aws_route_table.route-table-public-subnet2.id}"
}

# Tabla de rutas para la subred privada 1 

resource "aws_route_table" "route-table-private-subnet1" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat-gw.id}"
  }
  tags = {
    Name = "route-table-private-subnet1"
  }
}

resource "aws_route_table_association" "route-table-association-private-subnet1" {
  subnet_id      = "${aws_subnet.private-subnet1.id}"
  route_table_id = "${aws_route_table.route-table-private-subnet1.id}"
}

# Tabla de rutas para la subred privada 2 

resource "aws_route_table" "route-table-private-subnet2" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat-gw.id}"
  }
  tags = {
    Name = "route-table-private-subnet2"
  }
}

resource "aws_route_table_association" "route-table-association-private-subnet2" {
  subnet_id      = "${aws_subnet.private-subnet2.id}"
  route_table_id = "${aws_route_table.route-table-private-subnet2.id}"
}

# Tabla de rutas para la subred privada 3

resource "aws_route_table" "route-table-private-subnet3" {
  vpc_id = "${aws_vpc.main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.nat-gw.id}"
  }
  tags = {
    Name = "route-table-private-subnet3"
  }
}

resource "aws_route_table_association" "route-table-association-private-subnet3" {
  subnet_id      = "${aws_subnet.private-subnet3.id}"
  route_table_id = "${aws_route_table.route-table-private-subnet3.id}"
}



# Elastic IP para el NAT Gateway

resource "aws_eip" "eip" {
  vpc      = true
  tags = {
    Name = var.eip-name
  }
}

# NAT Gateway

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = "${aws_eip.eip.id}"
  subnet_id     = "${aws_subnet.public-subnet1.id}"

  tags = {
    Name = var.ngw-name
  }
}

# Security Group

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow http inbound traffic"
  vpc_id      = "${aws_vpc.main.id}"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  tags = {
		Name = "allow-http-sg"
	}
}
