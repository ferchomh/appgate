provider "aws" {
	access_key = "AKIAWNP6MXA5ODIYO3XX"
	secret_key = "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"
	region = "us-east-2"
}

variable "my_access_key" {
  description = "Access-key-for-AWS"
  default = "AKIAWNP6MXA5ODIYO3XX"
}
 
variable "my_secret_key" {
  description = "Secret-key-for-AWS"
  default = "bwEXuhnoLGEYA2QUFq/zwFMFpuBGWZaE6Y9haI8A"
}
 
variable "vpc_cidr" {
description = "CIDR for VPC"
default     = "10.1.96.0/24"
}

variable "public_subnet_cidr-01" {
description = "CIDR for public subnet-01"
default     = "10.1.96.0/27"
}

variable "private_subnet_cidr-01" {
description = "CIDR for private subnet-01"
default     = "10.1.96.64/27"
}

resource "aws_instance" "InstanceDocker" {
	ami = "ami-0231217be14a6f3ba"
	instance_type = "t2.micro"
 
	user_data = <<-EOF
			#!/bin/bash
			set -ex
			sudo yum update -y
			sudo amazon-linux-extras install docker -y
			sudo service docker start
			sudo usermod -a -G docker ec2-user
			sudo curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
			sudo chmod +x /usr/local/bin/docker-compose
			EOF
			
	tags = {
		Name = "My Instance Docker EC2 using Terraform"
	}
	vpc_security_group_ids = [aws_security_group.instance.id]
}
 
resource "aws_security_group" "instance" {
	
 
	ingress {
		from_port = 80
		to_port = 80
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
	
	ingress {
		from_port = 22
		to_port = 22
		protocol = "tcp"
		cidr_blocks = ["0.0.0.0/0"]
	}
 
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }
}
